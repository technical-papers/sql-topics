# Database topics

- ACID
- CAP Theorem
- Joins (should be covered in drills)
- Aggregations, Filters in queries (should be covered in Drills)
- Normalization
- Indexes
- Transactions
- Locking mechanism
- Database Isolation Levels
- Triggers


## ACID

  The acronym ACID refers to the four key properties of a transaction: atomicity, consistency, isolation, and durability.

  ### Atomicity
- All changes to data are performed as if they are a single operation. 
- That is, all the changes are performed, or none of them are.
- For example, in an application that transfers funds from one account to another, the atomicity property ensures that, if a debit is made successfully from one account, the corresponding credit is made to the other account.
  
### Consistency
- Data is in a consistent state when a transaction starts and when it ends.
- For example, in an application that transfers funds from one account to another, the consistency property ensures that the total value of funds in both the accounts is the same at the start and end of each transaction.
### Isolation
- The intermediate state of a transaction is invisible to other transactions. As a result, transactions that run concurrently appear to be serialized.
- For example, in an application that transfers funds from one account to another, the isolation property ensures that another transaction sees the transferred funds in one account or the other, but not in both, nor in neither.
### Durability
- After a transaction successfully completes, changes to data persist and are not undone, even in the event of a system failure.
- For example, in an application that transfers funds from one account to another, the durability property ensures that the changes made to each account will not be reversed.

![Acid properties](https://miro.medium.com/max/850/1*l-73Jy1Wk7NKrJnH-ZuHeQ.png)

## CAP theorem
The CAP Theorem is comprised of three components (hence its name) as they relate to distributed data stores:
 #### Consistency-
- Consistency means that all clients see the same data at the same time, no matter which node they connect to.
- For this to happen, whenever data is written to one node, it must be instantly forwarded or replicated to all the other nodes in the system before the write is deemed ‘successful.’
#### Availability-
 - Availability means that that any client making a request for data gets a response, even if one or more nodes are down.
 - all working nodes in the distributed system return a valid response for any request, without exception.
#### Partition tolerance-
- A partition is a communications break within a distributed system—a lost or temporarily delayed connection between two nodes.
- Partition tolerance means that the cluster must continue to work despite any number of communication breakdowns between nodes in the system.
![cap theorem](https://dz2cdn1.dzone.com/storage/temp/14001500-cap-theorum.png)

## Joins 
- SQL Join statement is used to combine data or rows from two or more tables based on a common field between them. 
- Different types of Joins are as follows:
  - INNER JOIN
  - LEFT JOIN
  - RIGHT JOIN
  - FULL JOIN
#### INNER JOIN (SIMPLE JOIN)-
- It is the most common type of SQL join. 
- SQL INNER JOINS return all rows from multiple tables where the join condition is met.
##### Syntax
The syntax for the INNER JOIN in SQL is:

       SELECT columns
       FROM table1 
       INNER JOIN table2
       ON table1.column = table2.column;

![inner join](https://www.techonthenet.com/sql/images/inner_join.gif)

#### LEFT JOIN (LEFT OUTER JOIN)-
- This join returns all the rows of the table on the left side of the join and matches rows for the table on the right side of the join.
   
- For the rows for which there is no matching row on the right side, the result-set will contain null. 
  
- LEFT JOIN is also known as LEFT OUTER JOIN.
##### Syntax
The syntax for the LEFT JOIN in SQL is:

       SELECT columns
       FROM table1
       LEFT JOIN table2
       ON table1.column = table2.column;

![LEFT JOIN](https://www.techonthenet.com/sql/images/left_outer_join.gif)


#### RIGHT JOIN (RIGHT OUTER JOIN)-
- This join returns all the rows of the table on the right side of the join and matching rows for the table on the left side of the join.
   
- For the rows for which there is no matching row on the left side, the result-set will contain null. 
  
- RIGHT JOIN is also known as RIGHT OUTER JOIN. 
##### Syntax
The syntax for the RIGHT JOIN in SQL is:

       SELECT columns
       FROM table1
       RIGHT JOIN table2
       ON table1.column = table2.column;

![RIGHT JOIN](https://www.techonthenet.com/sql/images/right_outer_join.gif)


#### FULL JOIN (FULL OUTER JOIN)-
- FULL JOIN creates the result-set by combining results of both LEFT JOIN and RIGHT JOIN.  
- The result-set will contain all the rows from both tables. 
   
- For the rows for which there is no matching, the result-set will contain NULL values. 
  
- FULL JOIN is also known as FULL OUTER JOIN. 
##### Syntax
The syntax for the FULL JOIN in SQL is:

       SELECT columns
       FROM table1
       FULL JOIN table2
       ON table1.column = table2.column;

![FULL JOIN](https://www.techonthenet.com/sql/images/full_outer_join.gif)

## Aggregations-
- In database management an aggregate function is a function where the values of multiple rows are grouped together as input on certain criteria to form a single value of more significant meaning.
  - Count()-
     - Count(*): Returns total number of records.
      - Count(salary): Return number of Non Null values over the column salary.
      - Count(Distinct Salary):  Return number of distinct Non Null values over the column salary.
  - Sum()-
    - sum(salary):  Sum all Non Null values of Column salary.
    - sum(Distinct salary): Sum of all distinct Non-Null values.
  - Avg()-
    - Avg(salary) = Sum(salary) / count
    - Avg(Distinct salary) = sum(Distinct
  - Min()-
    - Minimum value in the salary column except NULL
  - Max()
    - Maximum value in the salary 
## Filters
- SQL filters are text strings that you use to specify a subset of the data items in an internal or SQL database data type

- For SQL database and internal data types, the filter is an SQL WHERE clause that provides a set of comparisons that must be true in order for a data item to be returned. These comparisons are typically between field names and their corresponding values.


The SQL filter syntax supports the following comparative operators:

    >, <, =, <=, =>, !=, LIKE, AND, OR, NOT
- FILTER is a modifier used on an aggregate function to limit the values used in an aggregation. 
- All the columns in the select statement that aren’t aggregated should be specified in a GROUP BY clause in the query.
- The filter clause extends aggregate functions (sum, avg, count, …) by an additional where clause. The result of the aggregate is built from only the rows that satisfy the additional where clause too.
- The filter clause follows an aggregate function:

      SUM(<expression>) FILTER(WHERE <condition>)
      COUNT(*) FILTER (WHERE <condition>)
      COUNT(CASE WHEN <condition> THEN 1 END)

## Normalization
- Normalization is the process of organizing the data in the database.
- Normalization is used to minimize the redundancy from a relation or set of relations.
-  It is also used to eliminate undesirable characteristics like Insertion, Update, and Deletion Anomalies.
- Normalization divides the larger table into smaller and links them using relationships.
- The normal form is used to reduce redundancy from the database table.
- There are a few rules for database normalization. 
- Each rule is called a "normal form". 
- If the first rule is observed, the database is said to be in "first normal form". 
- If the first three rules are observed, the database is considered to be in "third normal form".
- Although other levels of normalization are possible, third normal form is considered the highest level necessary for most applications.
  ##### First normal form
    
    - Eliminate repeating groups in individual tables.
    - Create a separate table for each set of related data.
    - Identify each set of related data with a primary key.
    - A relation is in 1NF if it contains an atomic value.
  ##### Second normal form
    
    - Create separate tables for sets of values that apply to multiple records.
    - Relate these tables with a foreign key.
    - A relation will be in 2NF if it is in 1NF and all non-key attributes are fully functional dependent on the primary key.
  ##### Third normal form
    - Eliminate fields that do not depend on the key.
    -  	A relation will be in 3NF if it is in 2NF and no transition dependency exists.
  ##### BCNF
   - A stronger definition of 3NF is known as Boyce Codd's normal form.
  ##### Fourth normal form
   -  A relation will be in 4NF if it is in Boyce Codd's normal form and has no multi-valued dependency.
  ##### Fifth normal form
   -  A relation is in 5NF. If it is in 4NF and does not contain any join dependency, joining should be lossless.
### Advantages of Normalization
- Normalization helps to minimize data redundancy.
- Greater overall database organization.
- Data consistency within the database.
- Much more flexible database design.
- Enforces the concept of relational integrity.
### Disadvantages of Normalization
- The performance degrades when normalizing the relations to higher normal forms, i.e., 4NF, 5NF.
- It is very time-consuming and difficult to normalize relations of a higher degree.
- Careless decomposition may lead to a bad database design, leading to serious problems.
## Indexes
- Indexing is a data structure technique which allows you to quickly retrieve records from a database file. 
- An Index is a small table having only two columns. The first column comprises a copy of the primary or candidate key of a table. 
- Its second column contains a set of pointers for holding the address of the disk block where that specific key value stored.
- Indexing is defined based on its indexing attributes. 
- Indexing can be of the following types −
  #### Primary Index in DBMS
    - Primary Index is an ordered file which is fixed length size with two fields. 
    - The first field is the same a primary key and second, filed is pointed to that specific data block. 
    - In the primary Index, there is always one to one relationship between the entries in the index table.
    - The primary Indexing in DBMS is also further divided into two types.
      - Dense Index
      - Sparse Index
  ##### Dense Index
  - In a dense index, a record is created for every search key valued in the database.
  -  This helps you to search faster but needs more space to store index records. 
  -  In this Indexing, method records contain search key value and points to the real record on the disk.
  ##### Sparse Index
  - It is an index record that appears for only some of the values in the file. 
  - Sparse Index helps you to resolve the issues of dense Indexing in DBMS. 
  - In this method of indexing technique, a range of index columns stores the same data block address, and when data needs to be retrieved, the block address will be fetched.
  - However, sparse Index stores index records for only some search-key values. It needs less space, less maintenance overhead for insertion, and deletions but It is slower compared to the dense Index for locating records.
## Secondary Index in DBMS
- The secondary Index in DBMS can be generated by a field which has a unique value for each record, and it should be a candidate key. 
- It is also known as a non-clustering index.
- This two-level database indexing technique is used to reduce the mapping size of the first level. 
- For the first level, a large range of numbers is selected because of this; the mapping size always remains small.

## Clustering Index in DBMS
 - In a clustered index, records themselves are stored in the Index and not pointers. Sometimes the Index is created on non-primary key columns which might not be unique for each record. 
 - In such a situation, you can group two or more columns to get the unique values and create an index which is called clustered Index. This also helps you to identify the record faster.
## Multilevel Index

- Multilevel Indexing in Database is created when a primary index does not fit in memory.
 - In this type of indexing method, you can reduce the number of disk accesses to short any record and kept on a disk as a sequential file and create a sparse base on that file.
## B-Tree Index
- B-tree index is the widely used data structures for tree based indexing in DBMS. 
- It is a multilevel format of tree based indexing in DBMS technique which has balanced binary search trees. 
- All leaf nodes of the B tree signify actual data pointers.

Moreover, all leaf nodes are interlinked with a link list, which allows a B tree to support both random and sequential access.

### Advantages of Indexing

- It helps you to reduce the total number of I/O operations needed to retrieve that data, so you don’t need to access a row in the database from an index structure.
- Offers Faster search and retrieval of data to users.
- Indexing also helps you to reduce tablespace as you don’t need to link to a row in a table, as there is no need to store the ROWID in the Index. 
- Thus you will able to reduce the tablespace.
- You can’t sort data in the lead nodes as the value of the primary key classifies it.

### Disadvantages of Indexing

  - To perform the indexing database management system, you need a primary key on the table with a unique value.
  - You can’t perform any other indexes in Database on the Indexed data.
  - You are not allowed to partition an index-organized table.
  - SQL Indexing Decrease performance in INSERT, DELETE, and UPDATE query.

  

  ![Types of indexes](https://www.guru99.com/images/1/070119_0833_IndexinginD1.png)

## Transactions
- A transaction is a unit of work that is performed against a database. 
- Transactions are units or sequences of work accomplished in a logical order, whether in a manual fashion by a user or automatically by some sort of a database program.

- A transaction is the propagation of one or more changes to the database. For example, if you are creating a record or updating a record or deleting a record from the table, then you are performing a transaction on that table. 
- It is important to control these transactions to ensure the data integrity and to handle database errors.

- Practically, you will club many SQL queries into a group and you will execute all of them together as a part of a transaction.

### Properties of Transactions
Transactions have the following four standard properties, usually referred to by the acronym ACID.

  * Atomicity − ensures that all operations within the work unit are completed successfully. Otherwise, the transaction is aborted at the point of failure and all the previous operations are rolled back to their former state.

  * Consistency − ensures that the database properly changes states upon a successfully committed transaction.

  * Isolation − enables transactions to operate independently of and transparent to each other.

  * Durability − ensures that the result or effect of a committed transaction persists in case of a system failure.

![transaction](https://www.guru99.com/images/1/100518_0500_DBMSTransac1.png)

## Locking mechanism

- Locking protocols are used in database management systems as a means of concurrency control. 
- Multiple transactions may request a lock on a data item simultaneously. Hence, we require a mechanism to manage the locking requests made by transactions.
-  Such a mechanism is called as Lock Manager. 
-  It relies on the process of message passing where transactions and lock manager exchange messages to handle the locking and unlocking of data items.

### Data structure used in Lock Manager –
The data structure required for implementation of locking is called as Lock table.

  * It is a hash table where name of data items are used as hashing index.
  * Each locked data item has a linked list associated with it.
  * Every node in the linked list represents the transaction which requested for lock, mode of lock requested (mutual/exclusive) and current status of the request (granted/waiting).
  * Every new lock request for the data item will be added in the end of linked list as a new node.
  * Collisions in hash table are handled by technique of separate chaining.


### Explanation:
 In the above figure, the locked data items present in lock table are 5, 47, 167 and 15.

The transactions which have requested for lock have been represented by a linked list shown below them using a downward arrow.

Each node in linked list has the name of transaction which has requested the data item like T33, T1, T27 etc.

The colour of node represents the status i.e. whether lock has been granted or waiting.

Note that a collision has occurred for data item 5 and 47. It has been resolved by separate chaining where each data item belongs to a linked list. The data item is acting as header for linked list containing the locking request.

### Working of Lock Manager

1. Initially the lock table is empty as no data item is locked.
2. Whenever lock manager receives a lock request from a transaction Ti on a particular data item Qi following cases may arise:
   * If Qi is not already locked, a linked list will be created and lock will be granted to the requesting transaction Ti.
   * If the data item is already locked, a new node will be added at the end of its linked list containing the information about request made by Ti.
3. If the lock mode requested by Ti is compatible with lock mode of transaction currently having the lock, Ti will acquire the lock too and status will be changed to ‘granted’. Else, status of Ti’s lock will be ‘waiting’.
4. If a transaction Ti wants to unlock the data item it is currently holding, it will send an unlock request to the lock manager. The lock manager will delete Ti’s node from this linked list. Lock will be granted to the next transaction in the list.
5. Sometimes transaction Ti may have to be aborted. In such a case all the waiting request made by Ti will be deleted from the linked lists present in lock table. Once abortion is complete, locks held by Ti will also be released.


## Database Isolation Levels
- Isolation is one of the properties of SQL Transaction. 
- Isolating/separating transactions from each other to maintain Data Integrity in Database is called Isolation.
### Isolation Levels in SQL Server

- SQL Server provides 5 Isolation levels to implement with SQL Transaction to maintain data concurrency in the database.
 
- Isolation level is nothing but locking the row while performing some task, so that other transaction can not access or will wait for the current transaction to finish its job.

- Now, we will go through all the five Isolation levels 
1. Read Uncommitted
    * When this level is set, the transaction can read uncommitted data resulting in the Dirty Read problem. 
    * With this isolation level, we allow a transaction to read the data which is being updated by other transaction and not yet committed.
2. Read Committed
    * This prevents Dirty Read. When this level is set, the transaction can not read the data that is being modified by the current transaction. 
    * This will force user to wait for the current transaction to finish up its job
3. Repeatable Read
    * This level does every work that Read Committed does. but it has one additional benefit. User A will wait for the transaction being executed by User B to execute it's Update query as well, like Read Query. 
    * But Insert query doesn't wait, this also creates Phantom Read problem.
4. Snapshot
    * This level takes a snapshot of current data. Every transaction works on its own copy of data. 
    * When User A tries to update or insert or read anything, we ask him to re-verify the table row once again from the starting time of its execution, so that he can work on fresh data. 
    * with this level. We are not giving full faith to User A that he is going to work on fresh data but giving high-level changes of data integrity.
5. Serializable
    * This is the maximum level of Isolation level provided by SQL Server transaction. We can prevent Phantom Read problem by implementing this level of isolation. 
    * It asks User A to wait for the current transaction for any kind of operation he wants to perform.

- Isolation level also has a problem called "Dead Lock"- "Both the transactions lock the object and waits for each other to finish up the job". 
- DeadLock is very dangerous because it decreases the concurrency and availability of database and the database object



## Triggers
- Triggers are the SQL codes that are automatically executed in response to certain events on a particular table. 
- These are used to maintain the integrity of the data. 
- A trigger in SQL works similar to a real-world trigger. 
- For example, when the gun trigger is pulled a bullet is fired.

### Advantages and Disadvantages of Triggers
- Advantages

    * Forcing security approvals on the table that are present in the database
    * Triggers provide another way to check the integrity of data
    * Counteracting invalid exchanges
    * Triggers handle errors from the database layer
    * Normally triggers can be useful for inspecting the data changes in tables
    * Triggers give an alternative way to run scheduled tasks. Using triggers, we don’t have to wait for the scheduled events to run because the triggers are invoked automatically before or after a change is made to the data in a table
 


- Disadvantages


    * Triggers can only provide extended validations, i.e,  not all kind validations. For simple validations, you can use the NOT NULL, UNIQUE, CHECK and FOREIGN KEY constraints
    * Triggers may increase the overhead of the database
    * Triggers can be difficult to troubleshoot because they execute automatically in the database, which may not invisible to the client applications







## References
[Acid Properties](https://www.ibm.com/docs/en/cics-ts/5.4?topic=processing-acid-properties-transactions)

[Acid properties-link2](https://www.geeksforgeeks.org/acid-properties-in-dbms/)

[Acid properties-link3](https://databricks.com/glossary/acid-transactions)

[cap theorem](https://www.ibm.com/cloud/learn/cap-theorem)

[joins](https://www.geeksforgeeks.org/sql-join-set-1-inner-left-right-and-full-joins/)

[Aggregate Functions in SQL](https://www.geeksforgeeks.org/aggregate-functions-in-sql/?ref=lbp)

[Filter](https://modern-sql.com/feature/filter)

[Normalization](https://www.javatpoint.com/dbms-normalization)

[Normalization-link2](https://docs.microsoft.com/en-us/office/troubleshoot/access/database-normalization-description)

[indexes](https://www.guru99.com/indexing-in-database.html)
[transactions](https://www.guru99.com/dbms-transaction-management.html)
[Locking mechanism](https://www.geeksforgeeks.org/implementation-of-locking-in-dbms/)
[Database Isolation Levels](https://www.geeksforgeeks.org/transaction-isolation-levels-dbms/)
[Triggers](https://www.geeksforgeeks.org/sql-trigger-student-database/)